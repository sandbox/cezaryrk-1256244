<?php

class ViewsTreesUtil {
  
  static function display_render(&$view, $content_type = "application/json", $charset = "utf-8"){    
    // if argument is default then render normal 
    if(isset($view->view->style_plugin->is_default_argument)){
      $rendered = theme($view->theme_functions(), array('view' => $view->view));
    }else{
      // ajax callback
      // Return only data (html or json), don't render over theme system, this is an ajax callback
      // @todo response caching
      $rendered = !empty($view->view->result) || $view->view->style_plugin->even_empty() ? $view->view->style_plugin->render($view->view->result) : "";    
      drupal_add_http_header('Content-Type', "$content_type; charset=$charset");
      print $rendered;
      ajax_footer();    
      exit;    
    }
    return $rendered;
    
  }
  
  
  static function &get_handler(&$plugin, $type, $lookup_element){
    $handler =& $plugin->handler->get_handlers($type);
    if(isset($handler[$lookup_element])){
      return $handler[$lookup_element];
    }
    $null = null;
    return $null;
  }
}