(function($) {
	Drupal.behaviors.viewsTrees = {			
	  treeSettings : {},
	  mapViews : {},
	  attach: function(context,settings){	
		treeSettings = {};
		mapViews = {};
		if (!settings.viewsTrees){return;}		
		this.treeSettings = settings.viewsTrees;
		
		for(var i=0;i<this.treeSettings.length;i++){
			var tmp = this.treeSettings[i];
			var id = tmp.view.view_name+"-"+tmp.view.view_display_id;
			this.mapViews[id] = i;
			this.attachTree(context,tmp);		
		}
	  },
	
	   attachTree : function(context,ts){
			if(ts.style){
				// if(ts.style == "jstree"){this.attachJsTree(context, ts);}
				if(ts.style == "default"){this.attachDefaultTree(context, ts);}
			}		   
	   },
	   
	   attachDefaultTree : function(context,ts){
		   $(".tree-item .tree-collapse:not(.tree-processed)",context).addClass("tree-processed").click(function(event){
			   event.preventDefault();
			   $(this).viewsTreesCollapseChildren(ts);
		   });
	   }, 
	
	   attachJsTree : function(context,ts){
		   // TODO check if tree processed
		   var config = ts.style_config;
		   var callback = {
			    data : function (n) {
	    			var data = $.extend(true, {}, ts.view);		    			
		    		var pid = n.attr("id");
		    		if(pid != "undefined" && ((pid = pid.match(/jtr-(\d+)/)[1]) > 0)){ 
		    		   data.pid = pid;			    			
		    		}else{
 	    			   data.pid = "error";
 	    			   // TODO
			    	}				    	
		    		return data;
			    }
		   };
		   		   
		   $.extend(config.ext_html_data.ajax,callback);
		   var s = ".view-id-"+ts.view.view_name+".view-display-id-"+ts.view.view_display_id+" > .view-content";
		   $(s).jstree(config);		   
	   },
	   
	   getTreeSettings : function(view_name, view_display_id){
			var id = view_name+"-"+view_display_id;
			if(!(typeof this.mapViews[id] == 'undefined')){
				return this.treeSettings[this.mapViews[id]];
			}
			return false;		   
	   }
   };
	
		
	$.fn.viewsTreesCollapseChildren = function(ts){
		   var tc = $(this);
		   var ti = null;
		   if(tc.hasClass("tree-item")){
			   ti = tc;
			   tc = ti.find(".tree-collapse");
		   }else if(tc.hasClass("tree-collapse")){			   
			   ti = tc.closest(".tree-item");			   
		   }else{
			   // error
			   alert("error 101");
			   return;
		   }
		   
		   if(ti.hasClass("tree-closed")){
			   if(!ti.hasClass("tree-loaded")){
	   			   var data = $.extend(true, {}, ts.view); // clone object
	   			   // TODO ???? wird das noch benötigt ????
	   			   var node = tc.closest(".node");
	   			   if(node.length == 1){
	   				   // if the view is embedded in a node content
	   				   var nid = node.attr("id").match(/node-(\d+)/)[1];
	   				   data.view_args = nid + "/all";
	   			   }
	   			   //if(!data.pid){
		    	   var pid = ti.attr("id");
		    		if(pid != "undefined" && ((pid = pid.match(/ti-(\d+)/)[1]) > 0)){ 
		    		   data.pid = pid;			    			
		    		}else{
	    			   data.pid = "error";
			    	}			
	   			   //}
				   ti.addClass("tree-loading");
				   $.ajax({
					   url: ts.ajax_path,
					   data : data,
					   success : function(d){
						   ti.find(".tree-items").remove();
						   var tmp = $(d);
						   if(tmp.find(".tree-item").length > 0){
							   tmp.hide();
							   ti.children(".tree-placeholder").before(tmp);
							   tmp.fadeIn("fast");
							   Drupal.attachBehaviors(tmp);
							   ti.removeClass("tree-loading");
							   ti.addClass("tree-loaded");						   
							   ti.removeClass("tree-closed");
							   ti.addClass("tree-opened");	
						   }
					   },
					   error : function(e){ 
						   ti.removeClass("tree-loading"); 
						   alert("error"); 
					   }
				  });			   
			   }else{				   
				   ti.find(".tree-items").fadeIn("fast");
				   //ti.find(".tree-items").show("fast");					   
				   ti.removeClass("tree-closed");
				   ti.addClass("tree-opened");	
			   }
		   }else if(ti.hasClass("tree-opened")) {				   
			   ti.removeClass("tree-opened");				   
			   ti.addClass("tree-closed");	
			   ti.find(".tree-items").fadeOut("fast");
		   }else{
			   // tree empty?
			   
		   }
		   
		
	};
	
	$.fn.viewsTreesRoot = function(ts,callback){
		var $c = $(this);
		var data = $.extend(true, {}, ts.view);
		data.pid = 0;
		$.ajax({
		  url: ts.ajax_path,
		  data : data,
		  success : function(data){
		   var tmp = $(data);
		   $c.append(tmp);
		   Drupal.attachBehaviors(tmp);
		   // todo
		   callback();
	   },
					   
	   error : function(e){ 		 
		  alert("error 100"); 
	   }
	  });			   
		   
		
	};
	
	
})(jQuery);