<?php

class views_trees_style extends views_plugin_style {

  const ROOT_ARGUMENT = 'parent_argument';

  public $tree_style = "default"; 
  public $js_settings = array(); 
  public $style_css_class = "default";  
  public $parent_id = "all";
  
  

  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();
    $options[self::ROOT_ARGUMENT] = array('default' => '');
    // disable grouping
    $options["grouping"] = array('default' => false);    
    return $options;
  }

  
  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $arguments = array('' => t('<None>'));
    $this->get_field_labels_from_handlers('argument', $arguments);

    // TODO implement full and on demand modi, currently it is ondemand for large trees
    $form[self::ROOT_ARGUMENT] = array(
      '#type' => 'select',
      '#title' => t('Contextual filter for parent id'),
      '#options' => $arguments,
      '#default_value' => $this->options[self::ROOT_ARGUMENT],
      '#description' => t('Select the contextual filter'),
      '#required' => TRUE,
    );
  }

  
  function get_argument_with_parent_id(){
    return $this->options[self::ROOT_ARGUMENT];
  }
  
  function theme_functions(){
    $ret = parent::theme_functions();  
    return $ret;
  }
  
  function query(){    
    parent::query();
    
    $handler =& $this->get_handler("argument", $this->options[self::ROOT_ARGUMENT]);
    $handler_class = get_class($handler);    
    
    if(method_exists($this, "query_for_".$handler_class)) {      
      $this->{"query_for_".$handler_class}($handler);
    }else{
      // @todo what then???
    }
  }

  // @todo query for taxonomy!
  
  // Add Tree Root Query for References Handler
  function query_for_references_handler_argument(references_handler_argument &$handler){
    $handler->ensure_my_table();
    
    if($handler->is_default){
      $argument = $handler->get_default_argument();
      
      // @todo add exception value
      if($handler->is_exception($argument)){
        $null_check = "$handler->table_alias.$handler->real_field IS NULL";
        $handler->query->add_where_expression(0, $null_check);
      }      
    } else {
          
    }
    
    
    $reference_table = $handler->table;
    $base_table = $this->view->base_table;

    // copy handler definition and modify join condition
    $views_count_join = new views_join();
    $views_count_join->definition = $handler->query->table_queue[$handler->table]['join']->definition;
    $views_count_join->definition['field'] = $handler->real_field;
    $views_count_join->construct();

    $alias = $handler->query->add_table($reference_table,null,$views_count_join);

    $handler->query->fields['count_children'] = array();
    $handler->query->fields['count_children']['function'] = 'count';
    $handler->query->fields['count_children']['table'] = $alias;
    $handler->query->fields['count_children']['field'] = "entity_id";
    $handler->query->fields['count_children']['alias'] = "count_children";

    // TODO if nid is not set then add field for konsistens
    if(!isset($handler->query->fields['nid'])){
      $handler->query->fields['nid'] = array();
      $handler->query->fields['nid']['table'] = "node";
      $handler->query->fields['nid']['field'] = "nid";
      $handler->query->fields['nid']['alias'] = "nid";
    }

    $handler->query->add_groupby("nid");
  }


  // Handle comment reference
  function query_for_views_trees_handler_argument_comment_pid(views_trees_handler_argument_comment_pid &$handler){
    $handler->ensure_my_table();
     
    if($handler->is_default){
      $argument = $handler->get_default_argument();
      
      // @todo add exception value
      if($handler->is_exception($argument)){
        $null_check = "$handler->table_alias.$handler->real_field = 0";
        $handler->query->add_where_expression(0, $null_check);
      }
    }

    $reference_table = $handler->table;
    $base_table = $this->view->base_table;

    // copy handler definition and modify join condition
    $views_count_join = new views_join();
    $views_count_join->definition = array();
    $views_count_join->definition['table'] = $reference_table;
    $views_count_join->definition['field'] = "pid";
    $views_count_join->definition['left_table'] = $reference_table;
    $views_count_join->definition['left_field'] = "cid";
    $views_count_join->construct();

    $alias = $handler->query->add_table($reference_table,null,$views_count_join);

    $handler->query->fields['count_children'] = array();
    $handler->query->fields['count_children']['function'] = 'count';
    $handler->query->fields['count_children']['table'] = $alias;
    $handler->query->fields['count_children']['field'] = "cid";
    $handler->query->fields['count_children']['alias'] = "count_children";

    // TODO if nid is not set then add field for konsistens
    if(!isset($handler->query->fields['cid'])){
      $handler->query->fields['cid'] = array();
      $handler->query->fields['cid']['table'] = "comment";
      $handler->query->fields['cid']['field'] = "cid";
      $handler->query->fields['cid']['alias'] = "cid";
    }

    $handler->query->add_groupby("cid");
  }

  function get_html_selector(){
    return "views-tree-".$this->style_css_class;
  }
  
  function pre_render($result){
    // @todo add identifier class, but is this needed 
    $this->view->display_handler->set_option("css_class", $this->get_html_selector());
    parent::pre_render($result);    
  }
  
  function even_empty(){
    // @todo update definition even empty
    return true;
  }


  function render(){    
    if($this->view->ajax_callback){      
      $output = $this->render_callback(true);
      $header = $this->render_callback_http_header();
      foreach($header as $i => $value) drupal_add_http_header($value[0], $value[1]);
      print $output;
      ajax_footer();
      exit;      
    }else{
      
      return $this->render_callback(false);
    }
  }
  
  
  function add_javascript() { }
  
  function render_callback_http_header(){
    return array(array("Content-Type", "text/html; charset=utf-8"));
  }
    
  function render_callback($ajax = false){     
    if ($this->uses_row_plugin() && empty($this->row_plugin)) {
      debug('views_plugin_style_default: Missing row plugin');
      return;
    }

    if(!$ajax){
       $this->add_javascript();
    }
    
    $pid_field = "";
    switch($this->view->base_table){
      case 'node':$pid_field = "nid"; break;
      case 'comment':$pid_field = "cid"; break;
      default:
        debug('views_plugin_style_default: Wrong base');
      return;
    }
    
    // Group the rows according to the grouping field, if specified.
    $sets = $this->render_grouping($this->view->result, $this->options['grouping']);
    
    
    $output = '';
    foreach ($sets as $title => $records) {
      $rows = array();
      foreach ($records as $row_index => $row) {
        $this->view->row_index = $row_index;
    
        $rows[$row_index] = array();
        $rows[$row_index]['_raw'] = $row;
        $rows[$row_index]['count'] = $row->count_children;
        $rows[$row_index]['pid'] = $row->{$pid_field};
        $rows[$row_index]['data'] = "";
    
        if ($this->uses_row_plugin()) {          
          $rows[$row_index]["data"] = $this->row_plugin->render($row);
        }else{
          // should not happend, because row_plugin is online!
          foreach($this->rendered_fields[$row_index] as $k => $v){
            if($label_field == $k) continue;
            $rows[$row_index]['data'] .= "<div>".$v."</div>";
          }
        }
      }
    
      $output .= theme($this->theme_functions(),
      array('view' => $this->view, 'options' => $this->options, 'rows' => $rows, 'title' => $title));
    }
    
    if(empty($output)){      
      // @todo this never happens 
      $output = theme($this->theme_functions(),
      array('view' => $this->view, 'options' => $this->options, 'rows' => array(), 'title' => $title));
      
    }
    
    unset($this->view->row_index);
    return $output;
  }


  function get_field_labels_from_handlers($handler_type, &$fields){
    foreach ($this->display->handler->get_handlers($handler_type)  as $field => $handler) {
      if (method_exists($handler, "label") && $label = $handler->label()) {
        $fields[$field] = $label;
      } else {
        $fields[$field] = $handler->ui_name();
      }
    }
  }

  function &get_handler($type, $lookup_element){
    $handler =& $this->display->handler->get_handlers($type);
    if(isset($handler[$lookup_element])){
      return $handler[$lookup_element];
    }
    $null = null;
    return $null;
  }

  function get_handler_id($type, $lookup_element){
    $handler =& $this->display->handler->get_handlers($type);
    return array_search($lookup_element, array_keys($handler));
  }
  
}