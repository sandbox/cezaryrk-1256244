<?php

// @todo rename this sensefully
class views_trees_argument_default_empty extends views_plugin_argument_default {

  function init(&$view, &$argument, $options) {
    parent::init($view, $argument, $options);
    
    if(isset($_GET['pid'])){      
      $view->args[0] = $_GET['pid'];
    }
    
  }
  
  
  /**
   * Return the default argument.
   */
  function get_argument() {  	
    $value = $this->options['argument'];
    $exv = $this->argument->options['exception']['value'];    
    return empty($value) ? $exv : $value; // Non numeric value causes _build_arguments to break
  }
  
  function option_definition() {
    $options = parent::option_definition();
    $options['argument'] = array('default' => '');
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    $form['argument'] = array(
        '#type' => 'textfield',
        '#title' => t('Parent id'),
        '#default_value' => $this->options['argument'],
        '#description' => t("Id of the root element or nothing for NULL")
    );
  }
    
  function convert_options(&$options) {
    if (!isset($options['argument']) && isset($this->argument->options['default_argument_fixed'])) {
      $options['argument'] = $this->argument->options['default_argument_fixed'];
    }
  }

}
