<?php 



function template_preprocess_views_tree(&$vars){
  $view  = $vars['view'];
  $handler  = $vars['view']->style_plugin;

  
  // @todo if ajax is on 
  if(!$view->ajax_callback){  
    $path = drupal_get_path("module", "views_trees");
    drupal_add_js($path."/js/views_trees.js");
    drupal_add_css($path."/css/default/views_trees.css");
    
    $settings = array(
      "viewsTrees"=>array(
        array(
          'ajax_path' => url('views/ajax'),
    	  'selector' => ".".$handler->get_html_selector(),
          'view' => array(
    	  	"view_name" => $view->name,
          	"view_display_id" => $view->current_display,
    	  	'view_args' => check_plain(implode('/', $view->args)),
    	  	'view_path' => check_plain($_GET['q']),
          	'view_base_path' => $view->get_path(),
          ),
      	  "style" => $handler->tree_style,
      	  "style_config" => $handler->js_settings, 
      	)
      )
    );    
    drupal_add_js($settings, "setting");    
  }else{
    // reset title if ajax call
    $vars['title'] = ""; 
  }
  
  template_preprocess_views_view_unformatted($vars);

}

