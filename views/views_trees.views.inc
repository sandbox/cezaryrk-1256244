<?php


// @todo is a hack, because some problem with loading the theme file
include_once 'theme/theme.inc';

/**
 * Implements hook_views_data_alter().
 *
 * Add comment parent-ids as root elements
 */
function views_trees_views_data_alter(&$data){
  $data['comment']['pid_argument'] = array(
   'title' => t('Parent cid'),
    'help' => t('The comment id of the parent comment.'),
  	'argument' => array(
      'handler' => 'views_trees_handler_argument_comment_pid',   
      'name field' => 'parent_id', // the field to display in the summary.
      'field' => "pid",
      'numeric' => TRUE,
    )
  );
}

/**
* Implements hook_views_pre_view().
*
* appends the display_extender on tree
*/
function views_trees_views_pre_view(&$view){
  $id = $view->current_display;
  $display =& $view->display[$id];
  $style_plugin = $display->handler->get_option('style_plugin');
  if(preg_match("/^tree_.*_style$/",$style_plugin)) {
    // display handler is the current handler so add the extender there
    $view->display_handler->extender['tree_display_extender'] = new views_trees_display_extender($view, $view->display_handler->display);
  }
}


/**
 * Implementation of hook_views_plugins()
 */
function views_trees_views_plugins() {
  $path = drupal_get_path('module', 'views_trees')."/views";
  
  $plugin = array(
    'module' => 'views_trees',

    'display_extender' => array(
      'tree_display_extender' => array(
        'no ui' => true,
        'handler' => 'views_trees_display_extender',
  		'title' => t('Tree display extender'),
      ),
    ),
    'style' => array(
      'tree_default_style' => array(
        'title' => t('Tree style'),
        'help' => t('Display the results as a tree'),
        'handler' => 'views_trees_style',
        'path' => "$path",
        'theme' => 'views_tree',
        'theme file' => 'theme.inc',
        'theme path' => $path."/theme",
        'uses options' => TRUE,
    	'base' => array("node","comment"), // @todo add taxonomy
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses grouping' => FALSE,
  		'use ajax' => TRUE, // @todo doesn't work here
  		'even empty' => TRUE,        
        'type' => 'normal',         
  ),
  ),
    'argument default' => array(
      'tree_root_default' => array(
        'title' => t('Tree root value'),
        'handler' => 'views_trees_argument_default_empty',
  // @todo currently only for node refernece or comments 
  ),
  ),
  );

  return $plugin;
}




