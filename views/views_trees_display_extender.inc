<?php

class views_trees_display_extender extends views_plugin_display_extender {
  
  
  function __construct(&$view, &$display){
    parent::init($view, $display);
    
    // @todo modify display settings
    // * set ajax to true! and readonly
    // * disable grouping!!!
    
    //$display->set_option("use_ajax");
  }
  
  function pre_execute() {
    
    $this->view->ajax_callback = false;
    
    // @todo rename pid value
    if(isset($_GET['pid'])){
    
      $this->view->ajax_callback = true;
      $style_options = $this->display->handler->get_option('style_options');      
      $parent_field = $style_options[views_trees_style::ROOT_ARGUMENT];
      $handlers =& $this->display->handler->get_handlers("argument");
      
      // locate handler position
      $position = -1;
      foreach($handlers as $id =>$handler){
        $position++;
        if($id ==  $parent_field){
          break;
        }
      }      
      $i = $position;
      $this->view->args[$i] = $_GET['pid'];
    }
    
  }
  
}